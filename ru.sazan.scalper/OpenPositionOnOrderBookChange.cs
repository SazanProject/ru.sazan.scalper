﻿using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.scalper
{
    public class OpenPositionOnOrderBookChange
    {
        private Strategy strategy;
        private OrderBookContext orderBook;
        private ObservableQueue<Signal> signalQueue;
        private TradingDataContext tradingData;
        private Logger logger;

        public OpenPositionOnOrderBookChange(Strategy strategy,
            OrderBookContext orderBook,
            ObservableQueue<Signal> signalQueue,
            TradingDataContext tradingData,
            Logger logger) 
        {
            this.strategy = strategy;
            this.orderBook = orderBook;
            this.signalQueue = signalQueue;
            this.tradingData = tradingData;
            this.logger = logger;

            this.orderBook.OnQuotesUpdate += new SymbolDataHasBeenUpdatedNotification(OnChange);
        }

        private void OnChange(string symbol)
        {
            if (this.strategy.Symbol != symbol)
                return;

            if (StrategyPositionExists())
                return;

            if (UnfilledOrdersExists())
                return;

            Order lastOrder = GetLastFilledOrder();

            TradeAction action = GetAction(lastOrder);

            double limitPrice = GetLimitPrice(action);

            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), action, OrderType.Limit, limitPrice, 0, limitPrice);

            this.logger.Log(
                String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сгенерирован сигнал {2}", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name, 
                signal.ToString()));

            this.signalQueue.Enqueue(signal);
        }

        private Order GetLastFilledOrder()
        {
            try
            {
                return this.tradingData.GetFilledCloseOrders(this.strategy).Last();
            }
            catch
            {
                return null;
            }
        }

        private TradeAction GetAction(Order order)
        {
            if (order == null)
                return TradeAction.Buy;

            if (order.OrderType == OrderType.Stop)
                return order.TradeAction;

            return order.InverseAction();
        }

        private double GetLimitPrice(TradeAction action)
        {
            if (action == TradeAction.Buy)
                return this.orderBook.GetBidPrice(this.strategy.Symbol, 0);

            return this.orderBook.GetOfferPrice(this.strategy.Symbol, 0);
        }

        private bool StrategyPositionExists()
        {
            if (this.tradingData.GetAmount(this.strategy) != 0)
                return true;

            return false;
        }

        private bool UnfilledOrdersExists()
        {
            if (this.tradingData.GetUnfilled(this.strategy, OrderType.Limit).Count() != 0)
                return true;

            return false;
        }

    }
}
