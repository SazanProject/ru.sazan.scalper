﻿using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ru.sazan.trader.Utility;

namespace ru.sazan.scalper
{
    public class BreakOutOnBar:AddedItemHandler<Bar>
    {
        private Strategy strategy;
        private BarSettings barSettings;

        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private Logger logger;

        public BreakOutOnBar(Strategy strategy, DataContext tradingData, ObservableQueue<Signal> signalQueue, Logger logger)
            :base(tradingData.Get<ObservableCollection<Bar>>())
        {
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;

            this.barSettings = this.tradingData.Get<IEnumerable<BarSettings>>().SingleOrDefault(s => s.StrategyId == this.strategy.Id);
        }

        public override void OnItemAdded(Bar item)
        {
            if (this.barSettings == null)
                return;

            if (this.tradingData.PositionExists(this.strategy))
                return;

            if (this.tradingData.UnfilledExists(this.strategy, OrderType.Market))
                return;

            IEnumerable<Bar> bars =
                this.tradingData.Get<IEnumerable<Bar>>().GetNewestBars(this.barSettings.Symbol,
                this.barSettings.Interval,
                this.barSettings.Period);

            if (bars.Count() < this.barSettings.Period)
                return;

            TradeAction? action = GetTradeAction(bars);

            if (action == null)
                return;

            double price = action == TradeAction.Buy ? item.High : item.Low;

            Signal signal =
                new Signal(this.strategy,
                    BrokerDateTime.Make(DateTime.Now),
                    action.Value,
                    OrderType.Market,
                    price,
                    0,
                    0);

            string logMessage = String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сгенерирован сигнал {2}",
                DateTime.Now,
                this.GetType().Name,
                signal.ToString());

            this.logger.Log(logMessage);

            this.signalQueue.Enqueue(signal);
        }

        private Nullable<TradeAction> GetTradeAction(IEnumerable<Bar> bars)
        {
            if (bars.LastBarHasHighestHigh())
                return TradeAction.Buy;

            if (bars.LastBarHasLowestLow())
                return TradeAction.Sell;

            return null;
        }
    }
}
