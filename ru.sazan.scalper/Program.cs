﻿using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.smartcom;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ru.sazan.trader.Utility;

namespace ru.sazan.scalper
{
    class Program
    {
        private static MarketDataProvider marketDataProvider = new MarketDataProvider();
        private static RawTradingDataProvider rawTradingDataProvider = new RawTradingDataProvider();
        private static SymbolsDataProvider symbolsDataProvider = new SymbolsDataProvider();

        private static OrderManager orderManager = new BacktestOrderManager(TradingData.Instance, DefaultLogger.Instance);
        
        private static TraderBase traderBase =
            new TraderBase(orderManager);

        private static Strategy strategy = new Strategy(1, "Sample strategy", "ST46520-RF-01", "RIH4", 1);

        static void Main(string[] args)
        {
            TradingData.Instance.Get<ICollection<Strategy>>().Add(strategy);

            BarSettings barSettings = new BarSettings(strategy, "RIH4", 3600, 3);
            TradingData.Instance.Get<ICollection<BarSettings>>().Add(barSettings);

            BreakOutOnBar breakOnBar =
                new BreakOutOnBar(strategy,
                    TradingData.Instance,
                    SignalQueue.Instance,
                    DefaultLogger.Instance);

            StopLossOnBar stopLossOnBar =
                new StopLossOnBar(strategy,
                    100,
                    TradingData.Instance,
                    SignalQueue.Instance,
                    DefaultLogger.Instance);

            TakeProfitOnBar takeProfitOnBar =
                new TakeProfitOnBar(strategy,
                    100,
                    TradingData.Instance,
                    SignalQueue.Instance,
                    DefaultLogger.Instance);


            Transaction importBars =
                new ImportBarsTransaction(TradingData.Instance.Get<ObservableCollection<Bar>>(),
                    "bars.txt");

            importBars.Execute();
            while (true)
            {
                try
                {
                    string command = Console.ReadLine();

                    if (command == "stop")
                    {

                        break;
                    }

                    if (command == "pnl")
                    {
                        Console.WriteLine(String.Format("Реализованный профит и лосс составляет {0} пунктов",
                            TradingData.Instance.GetProfitAndLossPoints(strategy)));
                    }
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    DefaultLogger.Instance.Log(e.Message);

                }
            }
        }

    }
}
