﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.scalper.tests
{
    public class TickCounterHandler:AddedItemHandler<Tick>
    {
        public int TickCounter { get; set; }

        public TickCounterHandler(DataContext tradingData)
            : base(tradingData.Get<ObservableCollection<Tick>>()) { }

        public override void OnItemAdded(Tick item)
        {
            this.TickCounter++;
        }
    }
}
