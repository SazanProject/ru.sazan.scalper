﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using System.Collections.Generic;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader;
using ru.sazan.trader.Emulation;

namespace ru.sazan.scalper.tests
{
    [TestClass]
    public class FollowTrendOnTickTests:TraderBaseInitializer
    {

        private Strategy strategy;
        private OrderBookContext orderBook;

        [TestInitialize]
        public void Setup()
        {
            this.strategy = this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == 1);
            this.orderBook = new OrderBookContext();
            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick("RTS-12.13_FT", new DateTime(2013, 12, 3, 10, 0, 0), TradeAction.Buy, 138000, 3600));

            this.orderBook.Update(0, this.strategy.Symbol, 138000, 100, 138010, 50);

            FollowTrendOnTick handler =
                new FollowTrendOnTick(this.strategy, 
                    30,
                    50,
                    this.orderBook, 
                    this.tradingData, 
                    this.signalQueue, 
                    new NullLogger());
        }

        [TestMethod]
        public void open_long_position_on_tick_test()
        {
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Order>>().Count());

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick("RTS-12.13_FT", new DateTime(2013, 12, 3, 10, 0, 1), TradeAction.Sell, 138090, 3000));

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Order>>().Count());

            Order order = this.tradingData.Get<IEnumerable<Order>>().Last();
            Assert.AreEqual(TradeAction.Buy, order.TradeAction);
            Assert.AreEqual(OrderType.Limit, order.OrderType);
            Assert.AreEqual(138010, order.Price);
        }

        [TestMethod]
        public void open_short_position_on_tick_test()
        {
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Order>>().Count());

            this.tradingData.Get<ObservableCollection<Tick>>().Add(new Tick("RTS-12.13_FT", new DateTime(2013, 12, 3, 10, 0, 1), TradeAction.Sell, 137950, 3000));

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Order>>().Count());

            Order order = this.tradingData.Get<IEnumerable<Order>>().Last();
            Assert.AreEqual(TradeAction.Sell, order.TradeAction);
            Assert.AreEqual(OrderType.Limit, order.OrderType);
            Assert.AreEqual(138000, order.Price);
        }
    }
}
